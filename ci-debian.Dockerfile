FROM golang:latest as builder
LABEL version=0.0.2 author=paulo@canilho.net support=prom-cli_support@canilho.net
# Build
WORKDIR /src
ADD . .
RUN make build-trim OUTPUT_DIR=binaries

FROM debian:10-slim
WORKDIR /app
COPY --from=builder /src/binaries/prom-cli.linux .
ENV PROM_URL=${PROM_URL}
ENV PROM_AUTH_USERNAME=${PROM_AUTH_USERNAME}
ENV PROM_AUTH_PASSWORD=${PROM_AUTH_PASSWORD}
ENV PROM_AUTH_TOKEN=${PROM_AUTH_TOKEN}
CMD ["/app/prom-cli.linux"]
