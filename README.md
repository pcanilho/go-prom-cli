# <img alt="go-prom-cli logo" src="https://gitlab.com/uploads/-/system/project/avatar/28788428/go-prom-cli.png" height="100" width="100"> go-prom-cli (`prom-cli`)

[![View - Documentation](https://img.shields.io/badge/View-Documentation-blue?style=for-the-badge)](https://go-prom-cli.canilho.net/)

## Features

* Push metrics of types: Gauge, Counter,
  Histogram & Summary to a given Prometheus [PushGateway] supplied via an
  argument of via an environment variable.
* Authentication can be done via `BasicAuth`, `Bearer` token or omitted altogether.
* (BETA) Query metrics from a Prometheus
  instance.

## Artefacts

| **OS**        | **ARCH**      | **Version**       | **Link** | **SHA512** |
| ------------- |:-------------:| :-------------:   | -------------:| -------------:|
| `Darwin`      | `x64`         | `v0.0.2`          | [download](https://gitlab.com/pcanilho/go-prom-cli/-/jobs/artifacts/v0.0.2/raw/binaries/prom-cli.v0.0.2_darwin_amd64?job=prepare_job) | `24f8c322e6eece1e764e2626a81b9bad288821c704662eb39eb06333c27a933fe873a6303ebb77444c0dff76986d64aa0ecd9dec4750c725e666852999dc5191` |
| `Linux`       | `x64`         | `v0.0.2`          | [download](https://gitlab.com/pcanilho/go-prom-cli/-/jobs/artifacts/v0.0.2/raw/binaries/prom-cli.v0.0.2_linux_amd64?job=prepare_job) | `6f6777c5337e1f22acf2413f333cd721d1c34d5b33049783f3e41acf7377c95bc754269037836bc2ea0a2525d298ce84e50a9fd2278346e55c3e8ce717b06ef3` |
| `Windows`     | `x64`         | `v0.0.2`          | [download](https://gitlab.com/pcanilho/go-prom-cli/-/jobs/artifacts/v0.0.2/raw/binaries/prom-cli.v0.0.2_windows_amd64.exe?job=prepare_job) | `56f391ab666b3b235a28505d795ce42f99dac5aa02802b4b0e19c180f4dc794a07cbc4ed03e2d80a9f696a80c5abfde6b9b67b5b007bc66f586fd3d1b82994b6` |

## Requirements

* Go >= `1.14` (dev-only)
* `direnv` (optional) (dev-only)

## Usage

### Examples

* Push a `Gauge` metric with value `42`
```shell
$ prom-cli push gauge -j job_foo -n foo_metric 42
```

* Push a `Counter` metric with value `42`
```shell
$ prom-cli push counter -j job_foo -n foo_metric 42
```

* Push a `Gauge` metric with value `42`, label/value `foo/bar` & namespace `lorem`:
```shell
$ prom-cli push gauge -j job_foo -n foo_metric -l 'foo=bar' -s 'lorem' 42
```

* Via arguments:
```shell
Global Flags:
      --prom.url string        [PROM_URL] the prometheus push gateway instance URL
```

#### Note

By default, a label named `instance` with the `hostname` of the machine running this code is added to all metrics. This value can be overwritten by utilising the `--label` flag, as seen below:
```shell
$ prom-cli push ... -l 'instance=<my_instance>' ...
```

### Environment

* Via `direnv` + `.env` file:

1. Copy the sample environment file:

```shell
$ cp env.sample .env
```

2. Overwrite the values with the ones appropriate to your use-case.
3. Allow `direnv` to extract those values whenever you visit the folder that has the previously created `.env` file:

```shell
$ direnv allow
direnv: export +PROM_URL
```

## Installation

Via `go get`:

```bash
$ go get gitlab.com/pcanilho/go-prom-cli
```

Via `docker`:
```bash
$ docker pull gitlab.com/pcanilho/go-prom-cli:<version>
```

Via `stand-alone` package (binary):
```bash
curl -L -o prom-cli "https://gitlab.com/pcanilho/go-prom-cli/-/jobs/artifacts/<version>/raw/binaries/prom-cli.<version>_<os>_<arch>?job=prepare_job"
```

* Example for the version `0.0.2`, `darwin` & 64-bit:

```bash
curl -L -o prom_cli "https://gitlab.com/pcanilho/go-prom-cli/-/jobs/artifacts/v0.0.2/raw/binaries/prom-cli.v0.0.2_darwin_amd64?job=prepare_job"
```

Using it as a library:

```go
package main

import (
    prom_cli "gitlab.com/pcanilho/go-prom-cli"
)
...
```

## Building

For convenience’s sake, I have created a `Makefile` that can be used, as shown below, to create releases of this project
for a target operating system and architecture:

* Build for `Linux` + `amd64`:

```shell
$ make build
```

* Build for `Windows` + `x86`:

```shell
$ make build TARGET_OS=windows TARGET_ARCH=386
```

* Build with `cgo`:

```shell
$ make build CGO_ENABLED=1
```

* Build for all operating-systems + `x64`:

```shell
$ make build-all
```

* Build docker container:

```shell
$ make build-docker
```
