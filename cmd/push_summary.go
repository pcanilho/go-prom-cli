package cmd

import (
	_ "embed"
	"github.com/pkg/errors"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/spf13/cobra"
	"time"
)

var (
	objectivesParam map[string]string
	objectives      map[float64]float64
	maxAge          time.Duration
	ageBuckets      uint32
)

var summaryCmd = &cobra.Command{
	Use:   "summary",
	Short: "push metrics to prometheus of type [Summary]",
	PreRunE: func(cmd *cobra.Command, args []string) error {
		cm, err := convertToObjectives(objectivesParam)
		if err != nil {
			return errors.Wrapf(err, "unable to convert the supplied objective map")
		}
		objectives = cm
		return nil
	},
	RunE: func(cmd *cobra.Command, args []string) error {
		// Initialise metric
		promMetric = prometheus.NewSummary(prometheus.SummaryOpts{
			Name:       metricName,
			Namespace:  namespace,
			Subsystem:  subSystem,
			Objectives: objectives,
			MaxAge:     maxAge,
			AgeBuckets: ageBuckets,
			Help:       metricHelp,
		})

		promMetric.(prometheus.Summary).Observe(metricValue)
		return nil
	},
}

func init() {
	summaryCmd.Flags().StringToStringVarP(&objectivesParam, "objective", "o", nil, "summary quantiles/objectives (optional)")
	summaryCmd.Flags().DurationVarP(&maxAge, "max-age", "m", prometheus.DefMaxAge, "max age for which an observation stays relevant to the summary (optional)")
	summaryCmd.Flags().Uint32VarP(&ageBuckets, "age-buckets", "a", prometheus.DefAgeBuckets, "number of buckets used to exclude observations that are older than MaxAge from the summary (optional)")

}

func convertToObjectives(vMap map[string]string) (map[float64]float64, error) {
	out := make(map[float64]float64, len(vMap))
	for k, v := range vMap {
		if vF, err := extractFloat64Value(k); err != nil {
			return nil, err
		} else {
			if kF, err := extractFloat64Value(v); err != nil {
				out[kF] = vF
			}
		}
	}
	return out, nil
}
