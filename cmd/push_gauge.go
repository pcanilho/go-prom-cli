package cmd

import (
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/spf13/cobra"
)

var gaugeCmd = &cobra.Command{
	Use:   "gauge",
	Short: "push metrics to prometheus of type [Gauge]",
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(args) == 0 {
			return fmt.Errorf("please specify the metric value")
		}

		// Initialise metric
		promMetric = prometheus.NewGauge(prometheus.GaugeOpts{
			Name:      metricName,
			Namespace: namespace,
			Subsystem: subSystem,
			Help:      metricHelp,
		})

		promMetric.(prometheus.Gauge).SetToCurrentTime()
		promMetric.(prometheus.Gauge).Set(metricValue)
		return nil
	},
}
