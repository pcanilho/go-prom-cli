package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
)

var seriesCmd = &cobra.Command{
	Use:   "series",
	Short: "retrieves metrics values from prometheus of the type [Series]",
	RunE: func(cmd *cobra.Command, args []string) error {
		labelSet, warnings, err := v1api.Series(promCtx, args, start, end)
		if err != nil {
			return err
		}
		if warnings != nil && len(warnings) > 0 {
			fmt.Println(warnings)
		}

		for _, l := range labelSet {
			fmt.Println(l)
		}
		return nil
	},
}