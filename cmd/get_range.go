package cmd

import (
	"fmt"
	"github.com/pkg/errors"
	v1 "github.com/prometheus/client_golang/api/prometheus/v1"
	"github.com/spf13/cobra"
	"time"
)

var (
	step                 time.Duration
)

var rangeCmd = &cobra.Command{
	Use:   "range",
	Short: "retrieves metrics values from prometheus of the type [Range]",
	RunE: func(cmd *cobra.Command, args []string) error {
		// Construct range
		queryRange := v1.Range{
			Start: start,
			End:   end,
			Step:  step,
		}

		// Query API
		results, warnings, err := v1api.QueryRange(promCtx, args[0], queryRange)
		if err != nil {
			return errors.Wrapf(err, "unable to query prometheus with the query range [%s]", args[0])
		}

		if warnings != nil && len(warnings) > 0 {
			fmt.Println(warnings)
		}

		fmt.Println(results)
		return nil
	},
}

func init() {
	rangeCmd.Flags().DurationVarP(&step, "step", "t", time.Minute, "the range start time (optional)")
}
