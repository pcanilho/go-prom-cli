package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"log"
	"os"
	"strings"
)

var (
	name            = "prom-cli"
	version, commit string
)

// rootCmd represents the root application command
var rootCmd = &cobra.Command{
	Use:              name,
	Version:          fmt.Sprintf("v%s (%s)", version, commit),
	Short:            "A wrapper module for Prometheus PushGateway interactions",
	PersistentPreRun: func(cmd *cobra.Command, args []string) { overrideConfig() },
}

var (
	// Arguments
	promUrl                    string
	bearerToken                string
	authUsername, authPassword string
	verbose                    int
	skipVerifyTLS              bool
)

type envArg struct {
	ArgName, EnvName, Usage string
}

// envMap accumulates all the arguments and associated environment variable name as well at the command usage
var envMap = map[*string]envArg{
	&promUrl: {
		ArgName: "prom.url",
		EnvName: "PROM_URL",
		Usage:   "[PROM_URL] the Prometheus instance URL",
	},
	&authUsername: {
		ArgName: "auth.username",
		EnvName: "PROM_AUTH_USERNAME",
		Usage:   "[PROM_AUTH_USERNAME] username to be provided when doing basic auth with the Prometheus instance (exclusive usage with auth.token)",
	},
	&authPassword: {
		ArgName: "auth.password",
		EnvName: "PROM_AUTH_PASSWORD",
		Usage:   "[PROM_AUTH_PASSWORD] password to be provided when doing basic auth with the Prometheus instance (exclusive usage with auth.token)",
	},
	&bearerToken: {
		ArgName: "auth.token",
		EnvName: "PROM_AUTH_TOKEN",
		Usage:   "[PROM_AUTH_TOKEN] communicate with the Prometheus instance using a 'Bearer' token (exclusive usage with auth.username & auth.password)",
	},
}

func init() {
	viper.AutomaticEnv()

	// Verbosity
	rootCmd.PersistentFlags().CountVarP(&verbose, "verbose", "v", "verbosity level")

	// Skip-Verify TLS
	rootCmd.PersistentFlags().BoolVar(&skipVerifyTLS, "tls.skip-verify", false, "skip verify TLS certificates")

	// Environment
	for key, entry := range envMap {
		rootCmd.PersistentFlags().StringVar(key, entry.ArgName, "", entry.Usage)
		if err := viper.BindEnv(entry.EnvName, entry.ArgName); err != nil {
			log.Fatalln(err)
		}
	}

	// Add commands
	rootCmd.AddCommand(pushCmd)
	rootCmd.AddCommand(getCmd)
}

func Execute() error {
	return rootCmd.Execute()
}

func HelpCalled() bool {
	for _, arg := range os.Args {
		if strings.Contains(arg, "help") || arg == "-h" {
			return true
		}
	}
	return false
}

func VersionCalled() bool {
	for _, arg := range os.Args {
		if strings.Contains(arg, "version") || arg == "-v" {
			return true
		}
	}
	return false
}

func overrideConfig() {
	setIfNotEmptyAndNotSet(&promUrl)
}

func setIfNotEmptyAndNotSet(key *string) {
	mappedVal, found := envMap[key]
	if !found {
		return
	}
	viperVal := viper.GetString(mappedVal.EnvName)
	if len(strings.TrimSpace(viperVal)) != 0 && len(strings.TrimSpace(*key)) == 0 {
		*key = viperVal
	}
}

func usingAuth() bool {
	return len(strings.TrimSpace(authUsername)) != 0 || len(strings.TrimSpace(authPassword)) != 0
}

func usingBearerToken() bool {
	return len(strings.TrimSpace(bearerToken)) != 0
}
