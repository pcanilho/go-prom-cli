package cmd

import (
	"context"
	"fmt"
	"github.com/pkg/errors"
	"github.com/prometheus/client_golang/api"
	v1 "github.com/prometheus/client_golang/api/prometheus/v1"
	"github.com/prometheus/common/config"
	"github.com/spf13/cobra"
	"strings"
	"time"
)

var (
	v1api      v1.API
	promCtx    context.Context
	cancelFunc context.CancelFunc
)

var (
	startParam, endParam string
	start, end           time.Time
)

var getCmd = &cobra.Command{
	Use:   "get",
	Short: "retrieves metric values from prometheus",
	PersistentPreRunE: func(cmd *cobra.Command, args []string) (err error) {
		cmd.Root().PersistentPreRun(cmd, args)

		// Validate args
		if len(args) == 0 {
			return fmt.Errorf("please specify a metric")
		}

		// Parse params
		start, err = timeFromString(startParam)
		if err != nil {
			return err
		}

		end, err = timeFromString(endParam)
		if err != nil {
			return err
		}

		// Initialise client
		rtt := api.DefaultRoundTripper
		if usingAuth() {
			rtt = config.NewBasicAuthRoundTripper(authUsername, config.Secret(authPassword), "", rtt)
		} else if usingBearerToken() {
			rtt = config.NewAuthorizationCredentialsRoundTripper("Bearer", config.Secret(bearerToken), rtt)
		}

		c, err := api.NewClient(api.Config{
			Address:      promUrl,
			RoundTripper: rtt,
		})
		if err == nil {
			v1api = v1.NewAPI(c)
			// Healthy API?
			if err = ping(); err == nil {
				return nil
			}
		}
		return errors.Wrapf(err, "unable to create the API client for the address [%s]", promUrl)
	},
	PersistentPostRun: func(cmd *cobra.Command, args []string) {
		cancelFunc()
	},
}

func ping() error {
	_, warnings, err := v1api.Query(promCtx, "up", time.Now())
	if err != nil {
		return err
	}
	if warnings != nil && len(warnings) > 0 {
		fmt.Println(warnings)
	}
	return nil
}

func init() {
	rangeCmd.PersistentFlags().StringVarP(&startParam, "start", "s", "", "the range start time in RFC850 format (e.g. 'Monday, 02-Jan-06 15:04:05 MST') (the 'now' keyword is also supported)")
	rangeCmd.PersistentFlags().StringVarP(&endParam, "end", "e", "", "the range start time in RFC850 format (e.g. 'Monday, 02-Jan-06 15:04:05 MST'")

	promCtx, cancelFunc = context.WithTimeout(context.Background(), time.Second*30)

	// Add sub-commands
	getCmd.AddCommand(rangeCmd)
	getCmd.AddCommand(seriesCmd)
}

func timeFromString(v string) (time.Time, error) {
	if strings.TrimSpace(strings.ToLower(v)) == "now" {
		return time.Now(), nil
	}
	return time.Parse(time.RFC850, v)
}
