package cmd

import (
	"fmt"
	"github.com/pkg/errors"
	"github.com/prometheus/client_golang/api"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/push"
	"github.com/prometheus/common/config"
	"github.com/spf13/cobra"
	"net/http"
	"os"
	"strconv"
)

var (
	metricValue                      float64
	metricName, namespace, subSystem string
	jobName, metricHelp              string
	labels                           map[string]string
	promPusher                       *push.Pusher
	promMetric                       prometheus.Collector
)

var pushCmd = &cobra.Command{
	Use:   "push",
	Short: "push metrics to prometheus",
	PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
		cmd.Root().PersistentPreRun(cmd, args)

		// Validate args
		if len(args) == 0 {
			return fmt.Errorf("please specify a metric value")
		}

		if m, err := extractFloat64Value(args[0]); err != nil {
			return err
		} else {
			metricValue = m
		}

		// Initialise pusher
		promPusher = push.New(promUrl, jobName)
		if usingAuth() {
			promPusher.BasicAuth(authUsername, authPassword)
		} else if usingBearerToken() {
			cc := &http.Client{
				Transport: config.NewAuthorizationCredentialsRoundTripper(
					"Bearer", config.Secret(bearerToken), api.DefaultRoundTripper),
			}
			promPusher.Client(cc)
		}
		return nil
	},
	PersistentPostRunE: func(cmd *cobra.Command, args []string) error {
		promPusher.Collector(promMetric)
		// Push the instance label by default
		hostname, err := os.Hostname()
		if err != nil {
			promPusher.Grouping("instance", hostname)
		}
		// Override / set labels
		if labels != nil {
			for k, v := range labels {
				promPusher.Grouping(k, v)
			}
		}
		return errors.Wrapf(promPusher.Push(), "unable to push metric to the prometheus push gateway")
	},
}

func init() {
	// Mandatory
	pushCmd.PersistentFlags().StringVarP(&metricName, "name", "n", "", "the metric name")
	pushCmd.PersistentFlags().StringVarP(&jobName, "job", "j", "", "the metric job name")
	// Optional
	pushCmd.PersistentFlags().StringToStringVarP(&labels, "label", "l", nil, "the metric labels to identify the metric (optional)")
	pushCmd.PersistentFlags().StringVarP(&namespace, "namespace", "s", "", "the metric namespace (optional)")
	pushCmd.PersistentFlags().StringVarP(&subSystem, "subsystem", "u", "", "the metric subsystem (optional)")
	pushCmd.PersistentFlags().StringVarP(&metricHelp, "metric-help", "p", "", "the metric help message (optional)")

	// Add sub-commands
	pushCmd.AddCommand(counterCmd)
	pushCmd.AddCommand(gaugeCmd)
	pushCmd.AddCommand(histogramCmd)
	pushCmd.AddCommand(summaryCmd)
}

func extractFloat64Value(v string) (float64, error) {
	f, err := strconv.ParseFloat(v, 64)
	return f, errors.Wrapf(err, "unable to parse value [%s] as a float64 number", v)
}
