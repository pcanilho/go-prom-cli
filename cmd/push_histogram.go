package cmd

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/spf13/cobra"
)

var (
	buckets []float64
)

var histogramCmd = &cobra.Command{
	Use:   "histogram",
	Short: "push metrics to prometheus of type [Histogram]",
	RunE: func(cmd *cobra.Command, args []string) error {
		// Initialise metric
		promMetric = prometheus.NewHistogram(prometheus.HistogramOpts{
			Name:      metricName,
			Namespace: namespace,
			Subsystem: subSystem,
			Buckets:   buckets,
			Help:      metricHelp,
		})

		promMetric.(prometheus.Histogram).Observe(metricValue)
		return nil
	},
}

func init() {
	summaryCmd.Flags().Float64SliceVarP(&buckets, "buckets", "b", prometheus.DefBuckets, "defines the buckets into which observations are counted (optional")

}
