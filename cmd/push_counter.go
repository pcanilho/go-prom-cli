package cmd

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/spf13/cobra"
)

var counterCmd = &cobra.Command{
	Use:   "counter",
	Short: "push metrics to prometheus of type [Counter]",
	RunE: func(cmd *cobra.Command, args []string) error {
		// Initialise metric
		promMetric = prometheus.NewCounter(prometheus.CounterOpts{
			Name:      metricName,
			Namespace: namespace,
			Subsystem: subSystem,
			Help:      metricHelp,
		})

		promMetric.(prometheus.Counter).Add(metricValue)
		return nil
	},
}
