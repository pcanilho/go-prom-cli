# Target configuration
TARGET_OS		:= linux
TARGET_ARCH		:= amd64
CGO_ENABLED		:= 0

RELEASE			:= 0.0.2
SHA				:= $(shell git rev-list -1 HEAD)
DATE            := $(shell git log -1 --date=short --pretty=format:%cd)
APP_NAME		:= prom-cli
REP_PATH		:= gitlab.com/pcanilho/go-prom-cli
VPATH			:= $(REP_PATH)/cmd
DOCKER_TARGET   := registry.gitlab.com/pcanilho/go-prom-cli:$(RELEASE)

OUTPUT_DIR		:= binaries

ifeq ($(TARGET_OS),windows)
	APP_EXTENSION = .exe
endif

build:
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(TARGET_OS) GOARCH=$(TARGET_ARCH) \
		go build -trimpath \
			-ldflags "-X $(VPATH).name=$(APP_NAME) -X $(VPATH).version=$(RELEASE) -X $(VPATH).commit=$(SHA)" \
			-o $(OUTPUT_DIR)/$(APP_NAME).v$(RELEASE)_$(TARGET_OS)_$(TARGET_ARCH)$(APP_EXTENSION) -- main.go

build-trim:
	CGO_ENABLED=$(CGO_ENABLED) GOOS=$(TARGET_OS) GOARCH=$(TARGET_ARCH) \
    	go build -trimpath \
    		-ldflags "-X $(VPATH).name=$(APP_NAME) -X $(VPATH).version=$(RELEASE) -X $(VPATH).commit=$(SHA)" \
    		-o $(OUTPUT_DIR)/$(APP_NAME).$(TARGET_OS) -- main.go

build-docker:
	docker build -f ci-scratch.Dockerfile -t $(DOCKER_TARGET) .

build-docker-debian:
	docker build -f ci-debian.Dockerfile -t $(DOCKER_TARGET)-debian .

build-docker-all:
	make build-docker
	make build-docker-debian

build-all:
	for os in windows linux darwin; \
	do \
	 for arch in amd64; \
	 	do \
	 	make build TARGET_OS=$$os TARGET_ARCH=$$arch;\
	 done; \
	done;

docker-login:
	docker login registry.gitlab.com

publish-docker: docker-login
	docker push $(DOCKER_TARGET)
	docker push $(DOCKER_TARGET)-debian

publish-to-registry-ci: build-docker-all
	docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
	make publish-docker

doc:
	golds -gen -dir=./docs/generated -nouses -plainsrc -wdpkgs-listing=promoted ./...
