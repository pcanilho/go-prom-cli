package main

import (
	"gitlab.com/pcanilho/go-prom-cli/cmd"
	"os"
)

func main() {
	// Execute the root command
	if err := cmd.Execute(); err != nil {
		os.Exit(1)
	}

	// Exit gracefully if help was called
	if cmd.HelpCalled() || cmd.VersionCalled() {
		os.Exit(0)
	}
}